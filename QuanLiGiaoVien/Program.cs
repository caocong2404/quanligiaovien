﻿using System;
using System.Collections.Generic;
using TeacherManagement;
using TeacherManagement.util;
using static System.Runtime.InteropServices.JavaScript.JSType;

class Program
{
    static void Main(string[] args)
    {
        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        int soLuongGiaoVien = MyToys.GetInt("Nhap so luong giao vien: ", "So luong so vien phai co chu so", 1, int.MaxValue);
        
        for (int i = 0; i < soLuongGiaoVien; i++)
        {
            Console.Write($"\nNhap thong tin giao vien {i + 1}:");
            string hoTen = Console.ReadLine();
            int namSinh = MyToys.GetInt("Nam sinh: ", "Nam sinh phai nam trong khoang 1850..." + DateTime.Now.Year, 1850, DateTime.Now.Year);
            double luongCoBan = MyToys.GetDouble("Luong co ban: ", "Luong co ban khong hop le", 1, double.MaxValue);
            double heSoLuong = MyToys.GetDouble("He so luong: ", "He so luong khong hop le", 1, double.MaxValue);
            GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
            danhSachGiaoVien.Add(giaoVien);
        }
        if (danhSachGiaoVien.Count > 0)
        {
            GiaoVien giaoVienLuongThapNhat = danhSachGiaoVien[0];
            foreach (var giaoVien in danhSachGiaoVien)
            {
                if (giaoVien.TinhLuong() < giaoVienLuongThapNhat.TinhLuong())
                {
                    giaoVienLuongThapNhat = giaoVien;
                }
            }
            Console.WriteLine("\nThong tin giao vien co luong thap nhat:");
            giaoVienLuongThapNhat.XuatThongTin();
            Console.Read();
        }
        else
        {
            Console.WriteLine("Khong co giao vien nao trong danh sach.");
        }
    }
}