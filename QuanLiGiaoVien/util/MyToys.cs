﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeacherManagement.util
{
    static class MyToys
    {
        public static double GetDouble(string inputMsg, string errMsg, string exEr)
        {
            double n;
            while (true)
            {
                try
                {
                    Console.Write(inputMsg);
                    n = double.Parse(Console.ReadLine());
                    if (n > 0)
                    {
                        return n;
                    }
                    else
                    {
                        Console.WriteLine(exEr);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errMsg);
                }

            }
        }

        public static double GetDouble(String inputMsg, String errorMsg, double minValue, double maxValue)
        {
            double n;
            //swap
            if (minValue > maxValue)
            {
                double t = minValue;
                minValue = maxValue;
                maxValue = t;
            }

            while (true)
            {
                try
                {
                    Console.Write(inputMsg);
                    n = double.Parse(Console.ReadLine());
                    if (n < minValue || n > maxValue)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        return n;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errorMsg);
                }

            }
        }
        public static int GetInt(string inputMsg, string errMsg, string exEr)
        {
            int n;
            while (true)
            {
                try
                {
                    Console.Write(inputMsg);
                    n = int.Parse(Console.ReadLine());
                    if (n > 0)
                    {
                        return n;
                    }
                    else
                    {
                        Console.WriteLine(exEr);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errMsg);
                }
            }
        }

        public static int GetInt(String inputMsg, String errorMsg, int minValue, int maxValue)
        {
            int n;
            //swap
            if (minValue > maxValue)
            {
                int t = minValue;
                minValue = maxValue;
                maxValue = t;
            }

            while (true)
            {
                try
                {
                    Console.Write(inputMsg);
                    n = int.Parse(Console.ReadLine());
                    if (n < minValue || n > maxValue)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        return n;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(errorMsg);
                }

            }
        }

        private static bool CheckPositiveNum(int input)
        {
            bool result = false;
            if (input > 0)
            {
                result = true;
                return result;
            }
            return result;
        }
    }
}
